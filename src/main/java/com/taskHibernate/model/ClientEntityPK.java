package com.taskHibernate.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;

public class ClientEntityPK implements Serializable {

  private int id;
  private String login;

  @Column(name = "id")
  @Id
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Column(name = "login")
  @Id
  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    ClientEntityPK that = (ClientEntityPK) o;

    if (id != that.id) {
      return false;
    }
    if (login != null ? !login.equals(that.login) : that.login != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id;
    result = 31 * result + (login != null ? login.hashCode() : 0);
    return result;
  }
}
