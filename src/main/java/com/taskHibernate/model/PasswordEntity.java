package com.taskHibernate.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "password", schema = "mybankdb")
@IdClass(PasswordEntityPK.class)
public class PasswordEntity {

  private String password;
  private int clientId;
  private String clientLogin;

  @Basic
  @Column(name = "password")
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Id
  @Column(name = "client_id")
  public int getClientId() {
    return clientId;
  }

  public void setClientId(int clientId) {
    this.clientId = clientId;
  }

  @Id
  @Column(name = "client_login")
  public String getClientLogin() {
    return clientLogin;
  }

  public void setClientLogin(String clientLogin) {
    this.clientLogin = clientLogin;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    PasswordEntity that = (PasswordEntity) o;

    if (clientId != that.clientId) {
      return false;
    }
    if (password != null ? !password.equals(that.password) : that.password != null) {
      return false;
    }
    if (clientLogin != null ? !clientLogin.equals(that.clientLogin) : that.clientLogin != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = password != null ? password.hashCode() : 0;
    result = 31 * result + clientId;
    result = 31 * result + (clientLogin != null ? clientLogin.hashCode() : 0);
    return result;
  }
}
