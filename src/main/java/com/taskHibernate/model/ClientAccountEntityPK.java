package com.taskHibernate.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;

public class ClientAccountEntityPK implements Serializable {

  private int id;
  private int clientId;

  @Column(name = "id")
  @Id
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Column(name = "client_id")
  @Id
  public int getClientId() {
    return clientId;
  }

  public void setClientId(int clientId) {
    this.clientId = clientId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    ClientAccountEntityPK that = (ClientAccountEntityPK) o;

    if (id != that.id) {
      return false;
    }
    if (clientId != that.clientId) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id;
    result = 31 * result + clientId;
    return result;
  }
}
