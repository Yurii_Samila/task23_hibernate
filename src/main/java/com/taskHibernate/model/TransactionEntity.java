package com.taskHibernate.model;

import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "transaction", schema = "mybankdb")
public class TransactionEntity {

  private int id;
  private Timestamp time;
  private int clientAccountId;
  private int serviceAccountId;
  private int amount;

  @Id
  @Column(name = "id")
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Basic
  @Column(name = "time")
  public Timestamp getTime() {
    return time;
  }

  public void setTime(Timestamp time) {
    this.time = time;
  }

  @Basic
  @Column(name = "client_account_id")
  public int getClientAccountId() {
    return clientAccountId;
  }

  public void setClientAccountId(int clientAccountId) {
    this.clientAccountId = clientAccountId;
  }

  @Basic
  @Column(name = "service_account_id")
  public int getServiceAccountId() {
    return serviceAccountId;
  }

  public void setServiceAccountId(int serviceAccountId) {
    this.serviceAccountId = serviceAccountId;
  }

  @Basic
  @Column(name = "amount")
  public int getAmount() {
    return amount;
  }

  public void setAmount(int amount) {
    this.amount = amount;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    TransactionEntity that = (TransactionEntity) o;

    if (id != that.id) {
      return false;
    }
    if (clientAccountId != that.clientAccountId) {
      return false;
    }
    if (serviceAccountId != that.serviceAccountId) {
      return false;
    }
    if (amount != that.amount) {
      return false;
    }
    if (time != null ? !time.equals(that.time) : that.time != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id;
    result = 31 * result + (time != null ? time.hashCode() : 0);
    result = 31 * result + clientAccountId;
    result = 31 * result + serviceAccountId;
    result = 31 * result + amount;
    return result;
  }
}
