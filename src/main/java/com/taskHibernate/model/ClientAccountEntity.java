package com.taskHibernate.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "client_account", schema = "mybankdb")
@IdClass(ClientAccountEntityPK.class)
public class ClientAccountEntity {

  private int id;
  private String number;
  private int balance;
  private int clientId;

  @Id
  @Column(name = "id")
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Basic
  @Column(name = "number")
  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  @Basic
  @Column(name = "balance")
  public int getBalance() {
    return balance;
  }

  public void setBalance(int balance) {
    this.balance = balance;
  }

  @Id
  @Column(name = "client_id")
  public int getClientId() {
    return clientId;
  }

  public void setClientId(int clientId) {
    this.clientId = clientId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    ClientAccountEntity that = (ClientAccountEntity) o;

    if (id != that.id) {
      return false;
    }
    if (balance != that.balance) {
      return false;
    }
    if (clientId != that.clientId) {
      return false;
    }
    if (number != null ? !number.equals(that.number) : that.number != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id;
    result = 31 * result + (number != null ? number.hashCode() : 0);
    result = 31 * result + balance;
    result = 31 * result + clientId;
    return result;
  }
}
