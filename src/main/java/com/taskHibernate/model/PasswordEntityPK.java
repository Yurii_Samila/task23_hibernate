package com.taskHibernate.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;

public class PasswordEntityPK implements Serializable {

  private int clientId;
  private String clientLogin;

  @Column(name = "client_id")
  @Id
  public int getClientId() {
    return clientId;
  }

  public void setClientId(int clientId) {
    this.clientId = clientId;
  }

  @Column(name = "client_login")
  @Id
  public String getClientLogin() {
    return clientLogin;
  }

  public void setClientLogin(String clientLogin) {
    this.clientLogin = clientLogin;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    PasswordEntityPK that = (PasswordEntityPK) o;

    if (clientId != that.clientId) {
      return false;
    }
    if (clientLogin != null ? !clientLogin.equals(that.clientLogin) : that.clientLogin != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = clientId;
    result = 31 * result + (clientLogin != null ? clientLogin.hashCode() : 0);
    return result;
  }
}
