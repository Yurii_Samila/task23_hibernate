package com.taskHibernate;

import com.taskHibernate.model.CountryEntity;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

public class Main {

  private static SessionFactory sessionFactory;

  static {
    try {
      sessionFactory = new Configuration().configure().buildSessionFactory();
    } catch (Throwable ex) {
      throw new ExceptionInInitializerError(ex);
    }
  }

  public static Session getSession() throws HibernateException {
    return sessionFactory.openSession();
  }

  public static void main(String[] args) {
    Session session = getSession();
  try {
  findAllCountry(session);
 }finally {
  session.close();
  System.exit(0);
 }

  }

private static void findAllCountry(Session session){
  Query query = session.createQuery("from " + "CountryEntity");
  for (Object o : query.list()) {
    CountryEntity country = (CountryEntity) o;
    System.out.println(country);
  }
}

}
